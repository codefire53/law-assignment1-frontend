import axios  from "axios";

const baseAPI = axios.create({
    baseURL: "https://my-anime-db.herokuapp.com/api/"
})

const apiRequest = (method, url, data) => {
    const headers = {
      authorization: ""
    };
    //using the axios instance to perform the request that received from each http method
    return baseAPI({
        method,
        url,
        data,
        headers
      }).then(res => {
        return Promise.resolve(res.data);
      })
      .catch(err => {
        return Promise.reject(err);
      });
};

const get = (url, data) => apiRequest("get", url, data);
const post = (url, data) => apiRequest("post", url, data);
const put = (url, data) => apiRequest("put", url, data);
const deleteRequest = (url, data) => apiRequest("delete", url, data);

const API = {
    get,
    delete: deleteRequest,
    post,
    put
}

export default API;