import  API from '../services/Api'
export const getReviewsList = async () => {
    try {
        const response = await API.get("/get-reviews");
        console.log(response);
        return response;
    } catch(err) {
        console.log(err);
    }

}

export const getReviewDetail = async (id) => {
    try {
        const response = await API.get(`/get-review/${id}`);
        return response;
    } catch(err) {
        console.log(err);
        return err;
    }

}

export const updateReview = async (id,data) => {
    try {
        const response = await API.put(`/update-review/${id}`, data);
        return response;
    } catch(err) {
        console.log(err);
        return err.response;
    }
}

export const createReview = async(data) => {
    try {
        const response = await API.post(`/create-review`, data);
        return response;
    } catch(err) {
        console.log(err);
        return err.response;
    }
}